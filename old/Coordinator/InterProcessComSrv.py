from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from threading import Thread
import time
import sys
import json
from treq import post
from twisted.web.client import readBody


class HTTPJsonDataPoster():

    def handlePostResponse(self, response):
        #print(response.code)
        d = readBody(response)
        d.addCallback(self.handlePostResponseBody)
        return d

    def handlePostResponseBody(self, body):
        print('response body : %s ' % body)

    def sendJson(self, url, json_data):
        print('sending %s to %s' % (json.dumps(json_data), url))
        d=post(url=url, data=json.dumps(json_data).encode('utf-8'))
        d.addCallback(self.handlePostResponse)



class UdpCommandHandler(DatagramProtocol):
    def __init__(self):
        self.commands = {}

    def registerFunction(self,func_name, func):
        self.commands[func_name] = func

    def datagramReceived(self, data, host_port):
        host, port = host_port
        data=data.decode('utf-8').strip()
        # print("received %r from %s:%d" % (data, host, port))
        if "quit" == data:
            reactor.stop()
        else:
            # self.transport.write('cmd\n', (host, port))
            # messages are formated like this : func_name:arg1;arg2;arg3...
            if not ':' in data:
                return
            func_name, args = data.split(":")
            if ';' in args:
                args = args.split(';')
            else:
                if len(args) == 0:
                    args=None
            if func_name in self.commands.keys():
                # print('Calling func %s' % func_name)
                if args == None:
                    res = self.commands[func_name]()
                elif isinstance(args, list):
                    res = self.commands[func_name](*args)
                else:
                    res = self.commands[func_name](args)
                if res:
                    self.transport.write('result : {0}\n'.format(res).encode('ascii'), (host, port))
            else:
                self.transport.write('unknown command : {0}\n'.format(func_name).encode('ascii'), (host, port))

class CnxMgr(Thread):
    def __init__(self, port, cmd_handler):
        Thread.__init__(self, )
        self.port = port
        self.cmd_handler = cmd_handler

    def stop(self):
        print('CnxMgr : stopping reactor')
        reactor.stop()

    def run(self):
        print('CnxMgr : listening on port %s' % self.port)
        reactor.listenUDP(self.port, self.cmd_handler)
        reactor.run(installSignalHandlers=False)


if __name__ == '__main__':
    cmd_handler = UdpCommandHandler()
    h = HTTPJsonDataPoster()

    #cmd_handler.registerFunction('hello', hello_func)

    t = CnxMgr(9998, cmd_handler)
    t.daemon=True
    t.start()
    while True:
        print("blah")
        if not t.isAlive():
            sys.exit()
        h.sendJson('http://www.google.fr', {})
        time.sleep(0.1)


