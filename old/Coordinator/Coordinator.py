#!/usr/bin/python3

from math import sqrt
from PyQt5.QtCore import pyqtSignal, QObject
from PyQt5.QtCore import (QPointF, QRectF, qsrand,
        Qt, QTime)
from PyQt5.QtGui import (QColor, QPainter, QPen)
from PyQt5.QtWidgets import (QApplication, QGraphicsItem, QGraphicsScene,QGraphicsView, QPushButton)
from InterProcessComSrv import *

from random import randint


# Utilisation de l'interface graphique
# Clic droit sur un objet pour le supprimer
# Double clic au fond pour ajouter un Node
# Drag and Drop sur les Light et les Nodes
# Laisser la souris longtemps sur un Node ou une Light
# pour obtenir une infobulle contextuelle
# Utiliser la molette sur un objet pour influer sur sa couleur HSV
#       Utilisation des modifieurs Alt + Molette et Shift + Molette
#     Remarque : l'altération de la couleur sur un Noeud revient rapidement
#                à sa couleur d'origine car cette couleur est calculée en fonction
#                des Lights environnantes
#                pour désactiver cette dépendance aux Lights environnantes
#                Passer en mode "Autonome" (voir ci-dessous)
# Mode Autonome : Permet de désactiver l'influence des Lights sur les Nodes (surtout pour tester les couleurs
#   Le passage en mode Autonomme / Non Autonomme se fait en appuyant sur le Middle Button sur le Noeud de son choix
#   Les objets en mode Autonome sont décorés d'un contour Noir


class Communicate(QObject):
    """
    Used to emit / receive QT Signals from customs objects
    Each object have a "c" attribute of type Communicate
    - To emit a signal inside a class, we use self.c.SIGNAL.emit(self) :
        ie : self.c.rightClicked.emit(self)
    - To Map a QT Slot for a specific Signal, we would use OBJECT.c.SIGNAL.connect(CallBack_Function) :
        ie : lightInstance.c.lightPainted.connect(self.calculateColorsForAllNodes)
    """
    closeApp = pyqtSignal()
    lightPainted = pyqtSignal()
    nodePainted = pyqtSignal(object)
    rightClicked = pyqtSignal(object)

class LightningObject(QGraphicsItem):
    """
    This Class is almost an Abstract class to manage Objects represented by a Circle of a desired color.
    The object is QT Compatible and can be inserted on a QGraphicScene using its addItem(your_object) method
    """
    def __init__(self, name=None, x=None, y=None, size=None, h=None, s=None, v=None, color=None):
        super(LightningObject, self).__init__()
        if size==None:
            self.setSize(20)
        self.c = Communicate()  # Used for QT Signals emission
        self.name=name
        self.border = False  # Used to draw a border around the object by default no border is displayed.
        if x is None:
            x=randint(0, 255)  # arbitrary values if not initialized
        if y is None:
            y=randint(0, 255)  # arbitrary values if not initialized
        self.setXY(x, y)
        if color is not None:
            self.setQColor(color)
        elif not(h is None or s is None or v is None):
            self.setColorHSV(h, s, v)
        else:
            raise Exception("NO COLOR !!")

        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
        self.setCacheMode(QGraphicsItem.DeviceCoordinateCache)

    def setSize(self, size):
        self.width = size
        self.height = size


    def wheelEvent(self, event):
        """
        When a wheel Event occurs, we provide color changing of the Object.
        If the user holds Alt while using mouse wheel, it will modify object's Saturation
        If the user holds Shift while using mouse wheel, it will modify object's Value
        If nothing is holded, then the object's Hue is modified
        Note :  child class Node have its color influenced by child class Light.
                This mean that even if you use mouse wheel, color will revert back to the calculated color value.
                If you want to disable this, you will have to set the Node in Autonomous mode (clicking middle button on
                the node)
        """
        delta = event.delta()
        color = QColor()
        if delta > 0:
            direction = 1
        else :
            direction = -1
        modifiers = event.modifiers()
        h = self.color.hue()
        s = self.color.saturation()
        v = self.color.value()
        if modifiers == Qt.NoModifier:
            h = (h + (10*direction)) % 360
        elif modifiers == Qt.AltModifier:
            s = (s + (10*direction)) % 255
        elif modifiers == Qt.ShiftModifier:
            v = (v + (10*direction)) % 255
        elif modifiers == Qt.ControlModifier:
            self.width = self.width + (2*direction)
            self.height = self.height + (2*direction)

        # print ("%s %s %s" % (h,s,v))
        color.setHsv(h, s, v)
        self.setQColor(color)
        self.update()

    def setXY(self, x, y):
        self.setX(x)
        self.setY(y)
        self.update()

    def genToolTip(self):
        """
        :return: String representing usefull information to place in the ToolTip balloon of the current object
        """
        res = """
               <b>X, Y : </b>({X}, {Y})<br/>
               <b>R, G, B : </b>({Red}, {Green}, {Blue})<br/>
               <b>H, S, V : </b>({Hue}, {Saturation}, {Value})<br/>""".format(
                    X=self.x(),
                    Y=self.y(),
                    Red=self.color.red(),
                    Green=self.color.green(),
                    Blue=self.color.blue(),
                    Hue=self.color.hue(),
                    Saturation=self.color.saturation(),
                    Value=self.color.value(),
        )
        return res


    def setQColor(self, color):
        # We only do the color changing if the color passed as argument is different from the current one
        if not hasattr(self, "color") \
                or self.color.red() != color.red() \
                or self.color.green() != color.green() \
                or self.color.blue() != color.blue():
            self.color=color
            self.update()

    def setColorHSV(self, h, s, v):
        color = QColor(h=h, s=s, v=v)
        self.setQColor(color)

    def boundingRect(self):
        return QRectF(-self.width/2, -self.height/2, self.width, self.height)

    def paint(self, painter, option, widget):
        # Each time pain is called, objects information might have changes (coordinates, color, ...)
        # So we generate a new ToolTip text
        self.setToolTip(self.genToolTip())
        if self.border == True:
            painter.setPen(QPen(Qt.black, 0))
        else:
            painter.setPen(Qt.NoPen)
        painter.setBrush(self.color)
        painter.drawEllipse(-self.width/2, -self.height/2, self.width, self.height)


    def mousePressEvent(self, event):
        # should be handeled from the scene side
        # currently used to be able to remove objects by right clicking on them
        if event.button() == Qt.RightButton:
            self.c.rightClicked.emit(self)
        super(LightningObject, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        # Each time an object is moved, we need to update the color
        self.update()
        super(LightningObject, self).mouseReleaseEvent(event)



class Node(LightningObject):
    """
    Node Class represent LHaumpions
    Their color is changed by the PlaygroundWidget each time a Light is moved or each time
    the node is moved (except if setup in "Autonomous Mode")
    """
    def __init__(self, host_ip=None, name=None, host_port=80, x=None, y=None, h=None, s=None, v=None, color=None, size=None, json_data_poster=None):
        """

        """
        super().__init__(name, x=x, y=y, h=h, s=s, v=v, size=size, color=color)
        if size == None:
            self.setSize(20)
        if host_ip is None:
            host_ip = "SI.MU.LA.TION"
            self.online = False
        else:
            self.online = True
        if name is None:
            name = "Undefined (simulation)"
        self.name = name
        self.host_ip = host_ip
        self.host_port = host_port
        self.autonomous = False
        self.json_data_poster=json_data_poster
        if self.online:
            self._buildUrl()
            self.setQColor = self.setQColorOnline

    def setAutonomous(self,state): # Autonomous means not influenced by Lights arround (usefull for testing colors valued
        self.autonomous = state
        self.border = state

    def isAutonomous(self):
        return self.autonomous

    def mousePressEvent(self, event):
        if event.button() == Qt.MiddleButton:
            self.setAutonomous(not self.autonomous)
        super().mousePressEvent(event)


    def _buildUrl(self):
        self._host_url="HTTP://%s:%s/api/" % (self.host_ip, self.host_port)

    def setQColorOnline(self, color):
        if not hasattr(self, "color") or self.color.red() != color.red() or self.color.green() != color.green() or self.color.blue() != color.blue():
            super().setQColor(color)
            self.updateNodeColor()

    def genToolTip(self):
        res = """<b>Name : </b>{Nom}<br/>
               <b>IP : </b>{IP}<br/>""".format(
                    Nom=self.name,
                    IP=self.host_ip,
        )
        res += super().genToolTip()
        return res

    def updateNodeColor(self):
        data = {'led':255, 'rgb': [self.color.red(), self.color.green(), self.color.blue()]}
        self.json_data_poster.sendJson(self._host_url, data)
        #
        # req = Request(self._host_url)
        # req.add_header('Content-Type', 'application/json')
        # print("Envoie de %s à %s" % (data, self._host_url))
        # response = urlopen(req, json.dumps(data).encode('utf-8'))
        # print("Réponse : %s" % response.read())

    def getIdentifier(self):
        return id(self)

    def paint(self, painter, option, widget):
        super(Node, self).paint(painter, option, widget)
        if not self.autonomous:
            self.c.nodePainted.emit(self)

class Light(LightningObject):
    def __init__(self, name=None, x=None, y=None, h=None, s=None, v=None, size=None, color=None):
        super().__init__(name, x=x, y=y, h=h, s=s, v=v, size=size, color=color)
        self.width = 50
        self.height = 50

    def paint(self, painter, option, widget):
        super(Light, self).paint(painter, option, widget)
        self.c.lightPainted.emit()



class PlaygroundWidget(QGraphicsView):
    def __init__(self, json_data_poster):# for i in range(100):

        super(PlaygroundWidget, self).__init__()
        self.json_data_poster = json_data_poster
        self.margin_left = 20
        self.margin_right = 100
        self.margin_top = 40
        self.margin_bottom = 20

        scene = QGraphicsScene(self)
        scene.setItemIndexMethod(QGraphicsScene.NoIndex)
        scene.setSceneRect(0, 0, 600, 600)

        self.setScene(scene)
        self.setCacheMode(QGraphicsView.CacheBackground)
        self.setViewportUpdateMode(QGraphicsView.BoundingRectViewportUpdate)
        self.setRenderHint(QPainter.Antialiasing)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.NoAnchor)

        r = QColor()
        r.setRed(255)
        g = QColor()
        g.setGreen(255)
        b = QColor()
        b.setBlue(255)
        a = QColor()
        a.setBlue(100)
        a.setRed(100)
        for i in range(200):
            self.add_node()
        self.add_light(color=r)
        self.add_light(color=b)
        self.add_light(color=g)
        n = self.add_node(color=r, name="premier Lampion!", host_ip="jblb.net", host_port=8008)
        n.width=40
        n.height=40
        self.add_node( color=g)
        # self.add_node( color=b)
        # self.add_node( color=a)

        btn = QPushButton("Refresh");
        btn.clicked.connect(self.calculateColorsForAllNodes)
        btn.setGeometry(0,0,100,20)
        scene.addWidget(btn)

        btn = QPushButton("Add Light")
        btn.clicked.connect(self.add_light)
        btn.setGeometry(100,0,100,20)
        scene.addWidget(btn)

        btn = QPushButton("Add Node")
        btn.clicked.connect(self.add_node)
        btn.setGeometry(200, 0,100,20)
        scene.addWidget(btn)


        self.scale(1, 1)
        self.setMinimumSize(scene.width()+5, scene.height()+5)
        self.setWindowTitle("Lhaumpion's C&C")

    def mouseDoubleClickEvent(self, QMouseEvent):
        # print ("double click(%s, %s)"%(QMouseEvent.x(), QMouseEvent.y()))
        self.add_node(x=QMouseEvent.x(), y=QMouseEvent.y())

    def add_light(self, name=None, h=None, s=None, v=None, x=None, y=None, size=None,  color=None):
        if h is None or h == False:
            h=randint(0,359)
            s=100
            v=255
        if x is None:
            x=randint(0+ self.margin_left,self.width()-self.margin_right)
        if y is None:
            y=randint(0 + self.margin_top, self.height() - self.margin_bottom)
        # print ("add light(%s, %s, %s) " % (h, s, v))
        if color is None:
            color = QColor()
            color.setHsv(h, s, v, 255)
        l=Light(x=x, y=y, color=color, name=name, size=size)
        l.c.lightPainted.connect(self.calculateColorsForAllNodes)
        l.c.rightClicked.connect(self.remove_item)
        self.scene().addItem(l)

    def update_light(self, name, h=None, s=None, v=None, x=None, y=None, size=None):
        color = QColor()
        color.setHsv(h, s, v, 255)
        l = [item for item in self.scene().items() if isinstance(item, Light) and item.name == name]
        if len(l)<1:
            return 'Error %s not found !' % name
        else:
            l=l[0]
        l.setXY(x, y)
        l.setSize(size)
        l.setQColor(color)

    def reset_lights(self):
        lights_to_remove = [item for item in self.scene().items() if isinstance(item, Light)]
        for light in lights_to_remove:
            # print('removing light with id %s' % light)
            self.scene().removeItem(light)
        self.calculateColorsForAllNodes()


    def remove_light_with_name(self, name):
        lights_to_remove = [item for item in self.scene().items() if isinstance(item, Light) and item.name == name]
        for light in lights_to_remove:
            # print('removing light with id %s' % light)
            self.scene().removeItem(light)
        self.calculateColorsForAllNodes()

    def remove_item(self, item):
        # print('removing')
        self.scene().removeItem(item)
        self.calculateColorsForAllNodes()

    def add_node(self, h=None, s=None, v=None, x=None, y=None, color=None, host_ip=None, name=None, host_port=80):
        if h is None or h == False:
            h=randint(0,359)
            s=100
            v=200
        if x is None:
            x=randint(0+ self.margin_left,self.width()-self.margin_right)
        if y is None:
            y=randint(0 + self.margin_top, self.height() - self.margin_bottom)
        # print ("add node(%s, %s, %s) " % (h, s, v))
        if color is None:
            color = QColor()
            color.setHsv(h, s, v, 255)
        n=Node(x=x, y=y, color=color, host_ip=host_ip, name=name, host_port=host_port, json_data_poster=self.json_data_poster)
        n.c.nodePainted.connect(self.calculateColorsForNode)
        n.c.rightClicked.connect(self.remove_item)
        self.scene().addItem(n)
        return n


    def calculateColorsForAllNodes(self):
        # updating colors only on items no in autonomous mode
        nodes = [item for item in self.scene().items() if isinstance(item, Node) and not item.isAutonomous()]
        lights = [item for item in self.scene().items() if isinstance(item, Light)]
        for node in nodes:
            self.calculateColorsForNode(node, lights)

    # def calculateColorsForNode(self, node, lights=None):
    #     if lights is None:
    #         # print("calcul pour 1 Noeud")
    #         lights = [item for item in self.scene().items() if isinstance(item, Light)]
    #     if len(lights) < 1:
    #         return
    #     tmpColor = QColor(0,0,0)
    #     tmpCoefs = []
    #     dist_totale = 0
    #     # calcul du ratio fonction de la distance de chaque lampe pour le noeud courant
    #
    #     for light in lights:
    #         dist_totale += self.distance(node, light)
    #     for light in lights:
    #         tmpCoefs += [1-(1.0*self.distance(node, light)/dist_totale)]
    #     # print(tmpCoefs)
    #     r,g,b = (0, 0, 0)
    #     for i, light in enumerate(lights):
    #         r+= tmpCoefs[i] * light.color.red()
    #         g+= tmpCoefs[i] * light.color.green()
    #         b+= tmpCoefs[i] * light.color.blue()
    #     if len(lights) > 1:
    #         tmpColor.setRed(r * 1.0 / len(lights))
    #         tmpColor.setGreen(g * 1.0 / len(lights))
    #         tmpColor.setBlue(b * 1.0 / len(lights))
    #         tmpColor.setHsv(tmpColor.hue(),tmpColor.saturation(), 255 )
    #     else:
    #         tmpColor.setRed(light.color.red())
    #         tmpColor.setGreen(light.color.green())
    #         tmpColor.setBlue(light.color.blue())
    #     node.setQColor(tmpColor)

    def calculateColorsForNode(self, node, lights=None):
        if lights is None:
            # print("calcul pour 1 Noeud")
            lights = [item for item in self.scene().items() if isinstance(item, Light)]
        if len(lights) < 1:
            return
        # self.reset()
        tmpColor = QColor(0,0,0)
        (h, s, v) = (0, 255, 0)
        weightsum = 0
        maxweight = 0
        VIDEO_H=640
        VIDEO_W=480

        for light in lights:
            light_size = light.width #15
            dist = sqrt((light.x() - node.x()) ** 2 / VIDEO_W ** 2 + (light.y() - node.y()) ** 2 / VIDEO_H ** 2) / sqrt(2)
            weight = light_size / dist ** 2 / 1000

            if weight > 1: weight = 1
            if weight < 0: weight = 0

            weightsum += weight
            h += (light.color.hue() * weight)

            if maxweight < weight:
                v = maxweight = weight

        if weightsum > 0: h /= weightsum
        v *= 255
        tmpColor.setHsv(h,s,v)
        # 0;2print('h %s   s %s   v %s' % (h,s,v))
        node.setQColor(tmpColor)


    def distance(self,p1=QPointF(), p2=QPointF()):
        res=sqrt((p2.x()-p1.x())**2 + (p2.y()-p1.y())**2)
        return res

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Plus:
            self.scaleView(1.2)
        elif key == Qt.Key_Minus:
            self.scaleView(1 / 1.2)
        else:
            super(PlaygroundWidget, self).keyPressEvent(event)

    def timerEvent(self, event):
        nodes = [item for item in self.scene().items() if isinstance(item, Node)]

        for node in nodes:
            node.calculateForces()

        itemsMoved = False
        for node in nodes:
            if node.advance():
                itemsMoved = True

        if not itemsMoved:
            self.killTimer(self.timerId)
            self.timerId = 0

    # def wheelEvent(self, event):
    #     self.scaleView(math.pow(2.0, -event.angleDelta().y() / 240.0))

    def drawBackground(self, painter, rect):
        # Shadow.
        sceneRect = self.sceneRect()
        # rightShadow = QRectF(sceneRect.right(), sceneRect.top() + 5, 5, sceneRect.height())
        # bottomShadow = QRectF(sceneRect.left() + 5, sceneRect.bottom(), sceneRect.width(), 5)
        # if rightShadow.intersects(rect) or rightShadow.contains(rect):
	     #    painter.fillRect(rightShadow, Qt.darkGray)
        # if bottomShadow.intersects(rect) or bottomShadow.contains(rect):
	     #    painter.fillRect(bottomShadow, Qt.darkGray)
        painter.fillRect(sceneRect, Qt.darkGray)


    def scaleView(self, scaleFactor):
        factor = self.transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width()

        if factor < 0.07 or factor > 100:
            return

        self.scale(scaleFactor, scaleFactor)


if __name__ == '__main__':

    import sys


    def add_light(name, size, x, y, h, s, v):
        widget.add_light(x=int(float(x)), y=int(float(y)), h=int(float(h)), s=int(float(s)), v=int(float(v)), size=size, name=name)

    def update_light(name, size, x, y, h, s, v):
        widget.update_light(x=int(float(x)), y=int(float(y)), h=int(float(h)), s=int(float(s)), v=int(float(v)), size=int(float(size)), name=name)

    def remove_light(name):
        # print("removing %s" % name)
        widget.remove_light_with_name(name=name)

    def reset_light():
        widget.reset_lights()

    app = QApplication(sys.argv)
    qsrand(QTime(0,0,0).secsTo(QTime.currentTime()))

    json_data_poster = HTTPJsonDataPoster()

    cmd_handler = UdpCommandHandler()
    cmd_handler.registerFunction('add_light', add_light)
    cmd_handler.registerFunction('remove_light', remove_light)
    cmd_handler.registerFunction('update_light', update_light)
    cmd_handler.registerFunction('reset_lights', reset_light)
    json_data_poster = HTTPJsonDataPoster()

    t = CnxMgr(9998, cmd_handler)
    t.daemon=True
    t.start()

    widget = PlaygroundWidget(json_data_poster=json_data_poster)
    widget.show()

    app.exec_() # blocking until gui is exited
    t.stop() # stop the reactor
    sys.exit()
