CORDINATOR
==========


What is it
----------

This part of the code is a Python3 QT5 GUI

It provides a way to simulate the Vision part :
- you can add/remove/move Lights
- it will calculate Nodes color and update them
- it provides Connected Nodes (real ones with an IP Address) and 'Offline' Nodes (just for nicer rendering when you don't have much Nodes to play with)

It handles receiving UDP commands from the Vision Module so that the addition of Lights or their moving can be remotly controlled from the Vision Module
It handle the Network control of Connected Nodes (Lhaumpions).



INSTALLATION
------------
- It requires PYTHON3 (it was tested with python3-pyqt5)
- It requires PyQt5

I use ubuntus package python3-pyqt5 (i didn't manage to use pip3 to install pyqt5)

If it can help here is the result of a dpkg -l 'python3-*qt*' on my computer :
ii  python3-dbus.mainloop.pyqt5            5.2.1+dfsg-1ubuntu1      amd64                    D-Bus Support for PyQt5 with Python 3
ii  python3-dbus.mainloop.pyqt5-dbg        5.2.1+dfsg-1ubuntu1      amd64                    D-Bus Support for PyQt5 (debug extensions for Python 3)
ii  python3-pyqt5                          5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5
ii  python3-pyqt5-dbg                      5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5 (debug extensions)
ii  python3-pyqt5.qsci                     2.8.1-2ubuntu1           amd64                    Python 3 bindings for QScintilla 2 with Qt 5
ii  python3-pyqt5.qtmultimedia             5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's Multimedia module
ii  python3-pyqt5.qtmultimedia-dbg         5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's Multimedia module (debug extensions)
ii  python3-pyqt5.qtopengl                 5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's OpenGL module
ii  python3-pyqt5.qtopengl-dbg             5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's OpenGL module (debug extension)
ii  python3-pyqt5.qtpositioning            5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtPositioning module
ii  python3-pyqt5.qtpositioning-dbg        5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtPositioning module (debug extension)
ii  python3-pyqt5.qtquick                  5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtQuick module
ii  python3-pyqt5.qtquick-dbg              5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtQuick module (debug extension)
ii  python3-pyqt5.qtsensors                5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtSensors module
ii  python3-pyqt5.qtsensors-dbg            5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtSensors module (debug extension)
ii  python3-pyqt5.qtserialport             5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtSerialPort module
ii  python3-pyqt5.qtserialport-dbg         5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtSerialPort module (debug extension)
ii  python3-pyqt5.qtsql                    5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's SQL module
ii  python3-pyqt5.qtsql-dbg                5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's SQL module (debug extension)
ii  python3-pyqt5.qtsvg                    5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's SVG module
ii  python3-pyqt5.qtsvg-dbg                5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's SVG module (debug extension)
ii  python3-pyqt5.qtwebkit                 5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's WebKit module
ii  python3-pyqt5.qtwebkit-dbg             5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's WebKit module (debug extensions)
ii  python3-pyqt5.qtx11extras              5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtX11Extras module
ii  python3-pyqt5.qtx11extras-dbg          5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for QtX11Extras module (debug extension)
ii  python3-pyqt5.qtxmlpatterns            5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's XmlPatterns module
ii  python3-pyqt5.qtxmlpatterns-dbg        5.2.1+dfsg-1ubuntu1      amd64                    Python 3 bindings for Qt5's XmlPatterns module (debug extension)


- It also requires package treq (Twisted Requests) which depends on Twisted
Usage
-----

Just run it.
It should start a QT Gui and Listen on UDP Port 9998

To test UDP Communication use netcat utility (nc) : nc -u 127.0.0.1 9998
Usables commands : 
        reset_lights:
        add_light:NAME;X;Y;H;S;V
        update_light:NAME;X;Y;H;S;V
        remove_light:NAME

Example to past inside netcat UDP session :

reset_lights:
add_light:test;123;123;10;255;255
update_light:test;133;123;10;255;255
update_light:test;143;123;20;255;255
update_light:test;153;123;30;255;255
update_light:test;153;123;40;255;255
remove_light:test;123;123;10;255;255

GUI Usage (from source code)

# Utilisation de l'interface graphique
# Clic droit sur un objet pour le supprimer
# Double clic au fond pour ajouter un Node
# Drag and Drop sur les Light et les Nodes
# Laisser la souris longtemps sur un Node ou une Light
# pour obtenir une infobulle contextuelle
# Utiliser la molette sur un objet pour influer sur sa couleur HSV
#       Utilisation des modifieurs Alt + Molette et Shift + Molette
#     Remarque : l'altération de la couleur sur un Noeud revient rapidement
#                à sa couleur d'origine car cette couleur est calculée en fonction
#                des Lights environnantes
#                pour désactiver cette dépendance aux Lights environnantes
#                Passer en mode "Autonome" (voir ci-dessous)
# Mode Autonome : Permet de désactiver l'influence des Lights sur les Nodes (surtout pour tester les couleurs
#   Le passage en mode Autonomme / Non Autonomme se fait en appuyant sur le Middle Button sur le Noeud de son choix
#   Les objets en mode Autonome sont décorés d'un contour Noir


