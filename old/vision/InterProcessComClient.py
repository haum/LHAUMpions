import time
import sys
from threading import Thread
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

class AddLightUDPClientTest(DatagramProtocol):
    strings = [
        "add_light:test;123;123;10;255;255",
        "update_light:test;133;123;10;255;255",
        "update_light:test;143;123;20;255;255",
        "update_light:test;153;123;30;255;255",
        "update_light:test;153;123;40;255;255",
        "remove_light:test",
    ]


    def startProtocol(self):
        import time
        self.transport.connect('127.0.0.1', 9998)
        self.sendDatagram()
        time.sleep(5)
        self.sendDatagram()
        time.sleep(1)
        self.sendDatagram()
        time.sleep(1)
        self.sendDatagram()
        time.sleep(1)
        self.sendDatagram()
        reactor.stop()

    def sendDatagram(self):
        datagram = self.strings.pop(0)
        print "sending : %s" % datagram
        self.transport.write(datagram)

class LightStateSynchronizer(DatagramProtocol):
    def __init__(self, port):
        self.port = port
    #
    def startProtocol(self):
        self.transport.connect('127.0.0.1', self.port)

    def add_light(self, name, size, x, y, h, s, v):
        datagram = 'add_light:%s;%s;%s;%s;%s;%s;%s' % (name, size, x, y, h, s, v)
        self.sendDatagram(datagram)

    def update_light(self, name, size, x, y, h, s, v):
        datagram = 'update_light:%s;%s;%s;%s;%s;%s;%s' % (name, size, x, y, h, s, v)
        self.sendDatagram(datagram)

    def remove_light(self, name):
        datagram = 'remove_light:%s' % (name)
        self.sendDatagram(datagram)

    def reset_lights(self):
        datagram = 'reset_lights:'
        self.sendDatagram(datagram)

    def sendDatagram(self, datagram):
        # print "sending %s to (%s:%s)" % (datagram, "127.0.0.1", self.port)
        self.transport.write(datagram)

    def datagramReceived(self, data, host_port):
        host, port = host_port
        data=data.decode('utf-8').strip()
        print("received %r from %s:%d" % (data, host, port))

    def stop(self):
        reactor.stop()




class CnxMgrClient(Thread):
    def __init__(self, cmd_handler):
        Thread.__init__(self, )
        self.cmd_handler = cmd_handler

    def stop(self):
        print('CnxMgrClient : stopping reactor')
        reactor.stop()

    def run(self):
        reactor.listenUDP(0, self.cmd_handler)
        reactor.run(installSignalHandlers=False)


if __name__ == '__main__':
    cmd_handler = LightStateSynchronizer(9998)
    t = CnxMgrClient(cmd_handler)
    t.daemon=True
    t.start()
    time.sleep(3)
    for i in range(10):
        print("blah")
        if not t.isAlive():
            sys.exit()
        if i ==0:
            cmd_handler.add_light('toto', 100,100,50,255,255)
        if i ==1:
            cmd_handler.update_light('toto', 110,100,50,255,255)
        if i ==2:
            cmd_handler.update_light('toto', 120,100,60,255,255)
        if i ==3:
            cmd_handler.update_light('toto', 120,100,70,255,255)
        if i ==4:
            cmd_handler.update_light('toto', 120,100,80,255,255)
        time.sleep(3)

    # protocol = AddLightUDPClientTest()
    # t = reactor.listenUDP(0, protocol)
    # reactor.run()