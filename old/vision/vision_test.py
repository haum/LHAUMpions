#!/usr/bin/env python2

import cv2
import sys, random
import numpy as np
from math import sqrt

#################
# Choose camera #
#################

camera = ' '.join(sys.argv[1:])
if camera == '':
	camera = 0

######################
# Open video capture #
######################
 
VIDEO_W = 640
VIDEO_H = 480
cap = cv2.VideoCapture(camera)
cap.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, VIDEO_W)
cap.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, VIDEO_H)

##############
# LHAUMpions #
##############
class LHAUMpion:
	def __init__(self, x, y):
		self.x = x * VIDEO_W
		self.y = y * VIDEO_H
		self.objects = []
		self.reset()

	def reset(self):
		self.hue = 0
		self.value = 0

	def resetObjects(self):
		del self.objects[:]

	def addObject(self, obj):
		self.objects.append(obj)

	def updateColor(self):
		self.reset()
		weightsum = 0
		maxweight = 0

		for obj in self.objects:
			dist = sqrt((obj.pt[0] - self.x)**2/VIDEO_W**2 + (obj.pt[1] - self.y)**2/VIDEO_H**2)/sqrt(2)
			weight = obj.size/dist**2 / 1000

			
			if weight > 1 : weight = 1
			if weight < 0 : weight = 0

			weightsum += weight
			self.hue += (obj.hue * weight) 

			if maxweight < weight : 
				self.value = maxweight = weight

		if weightsum > 0 : self.hue /= weightsum







lhaumpions = []
for i in range(10):
	lhaumpions.append(LHAUMpion(random.random(), random.random()))

#########################
# Circle detection init #
#########################

# Parameters instance
params = cv2.SimpleBlobDetector_Params()

# Change thresholds
params.minThreshold = 100
params.maxThreshold = 240

# Filter by Area
params.filterByArea = True
params.minArea = 100
params.maxArea = 1000000

# Filter by Circularity
params.filterByCircularity = True
params.minCircularity = 0.80
 
# Filter by Convexity
params.filterByConvexity = True
params.minConvexity = 0.90
params.maxConvexity = 1
 
# Create a detector with the parameters
detector = None
ver = (cv2.__version__).split('.')
if int(ver[0]) < 3 :
    detector = cv2.SimpleBlobDetector(params)
else : 
    detector = cv2.SimpleBlobDetector_create(params)

#######################
# Interactive objects #
#######################
class InteractiveObject:
	def __init__(self, pt, size, hue):
		self.update(pt, size, hue)

	def update(self, pt, size, hue):
		self.pt = pt
		self.size = size
		self.hue = hue
		self.alive = 1

	def areYou(self, pt, hue):
		return ((self.pt[0]-pt[0])**2 + (self.pt[1]-pt[1])**2 < (3*self.size)**2) and (abs(self.hue - hue) < 0.1)

	def age(self):
		self.alive -= 0.1
		if self.alive <= 0: self.alive = 0
alives = []


#################
# Process video #
#################

while cv2.waitKey(10) == -1:
	# Read next image
	ret, im = cap.read()

	# Find circles
	keypoints = detector.detect(im)

	# Update alive objects
	for kp in keypoints:
		found = False
		hue = float(cv2.cvtColor(np.uint8([[im[kp.pt[1]][kp.pt[0]]]]), cv2.COLOR_BGR2HSV)[0][0][0])/180
		for a in alives:
			if a.areYou(kp.pt, hue):
				a.update(kp.pt, int(kp.size), hue)
				found = True
		if not found:
			alives.append(InteractiveObject(kp.pt, int(kp.size), hue))

	# Draw, age and kill-olds alives
	for a in alives:
		bgr = cv2.cvtColor(np.uint8([[[int(a.hue * 180), 255, 255]]]), cv2.COLOR_HSV2BGR)[0][0]
		bgr = (int(bgr[0]), int(bgr[1]), int(bgr[2]))
		cv2.ellipse(
			im,
			(int(a.pt[0]), int(a.pt[1])),
			(a.size, a.size),
			-90,
			0, 360*a.alive,
			bgr,
			4
		)
		
		a.age()
		if a.alive == 0:
			alives.remove(a)


	for l in lhaumpions:

		l.resetObjects()
		for a in alives:
			l.addObject(a)
		l.updateColor()


	# Draw lhaumpions
	for l in lhaumpions:

		bgr = cv2.cvtColor(np.uint8([[[int(l.hue * 180), 255, l.value*255]]]), cv2.COLOR_HSV2BGR)[0][0]
		bgr = (int(bgr[0]), int(bgr[1]), int(bgr[2]))
		cv2.circle(
			im,
			(int(l.x), int(l.y)),
			10,
			bgr,
			20
		)
		l.reset()


	# Show results
	cv2.imshow('Vision', im)
