
LHAUMpions ESP8266
==================

Ce répertoire contient le code source du micrologiciel des ESP8266 utilisés dans les LHAUMpions du projet "Lampes Orbitales".

Pré-requis :
  * [Arduino](http://arduino.cc) en version (>= 1.6.6)
  * [Support de l'ESP8266 pour IDE Arduino](https://github.com/esp8266/Arduino)
  * Les bibliothèques Arduino nécessaires au projet (installables via le gestionnaire de librairies de l'IDE)
    * [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
    * [FastLED](https://github.com/FastLED/FastLED)
Note: à l'heure où sont écrites ces lignes, il faut la branche master de FastLED, téléchargée en utilisant *Git* dans le répertoire ~/Arduino/libraries/

Pour compiler :
  * Copier *LHAUMpions.h.sample* vers *LHAUMpions.h*
  * Ouvrir le fichier ino dans l'IDE
  * Modifier les valeurs du fichier *LHAUMpions.h* pour le paramétrage du Wi-Fi
  * Choisir la Board qui convient *Generic ESP8266*
  * Choisir le port de connection du circuit */dev/ttyUSB0*
  * Compiler et télécharger le programme dans le module.
