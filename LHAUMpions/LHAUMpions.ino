#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#include <FastLED.h>
#include <ArduinoJson.h>	// https://github.com/bblanchon/ArduinoJson

#include "LHAUMpions.h"

#define DEBUGGING(...) Serial.println( __VA_ARGS__ )
#define DEBUGGING_L(...) Serial.print( __VA_ARGS__ )

#define API_VERSION		"0.2"

ESP8266WebServer HTTP(80);

char hostString[20] = { 0 };	// for hostname and mDNS responder

CRGB leds[PIXEL_COUNT];

void setup_wifi()
{
    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(wifi_ssid);
    WiFi.begin(wifi_ssid, wifi_password);
    while (WiFi.status() != WL_CONNECTED) {
	delay(500);
	Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

/*
 * HTTP
 *
 */

void handleRoot()
{
    Serial.println("handleRoot...");
    HTTP.send(200, "text/html", "<html>\
	<head>\
	    <style>\
	      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
	    </style>\
	</head>\
  <body>\
    <h1>It's works</h1>\
  </body>\
</html>");
}

void handleAPI()
{
    StaticJsonBuffer < 200 > jsonBuffer;	// Reserve memory space for json
    String json;
    DEBUGGING("handle API");
    if (HTTP.method() == HTTP_GET) {
	// reciving a GET -> Send Name & API version
	JsonObject & root = jsonBuffer.createObject();
	root["Name"] = hostString;
	root["API"] = API_VERSION;
	// root.prettyPrintTo(Serial); // print it
	char buffer[256];
	root.printTo(buffer, sizeof(buffer));
	HTTP.send(200, "text/plain", buffer);
	return;
    } else {
	if (HTTP.hasArg("plain")) {
	    json = HTTP.arg("plain");
	}
	DEBUGGING(json);
	// Deserialize the JSON string
	//
	JsonObject & root = jsonBuffer.parseObject(json);
	if (!root.success()) {
	    Serial.println("parseObject() failed");
	    HTTP.send(400, "text/plain", "parseObject() failed");
	    return;
	}
	//
	// Retrieve the values
	//
	int led = root["led"];
	int rgb_r;
	int rgb_g;
	int rgb_b;
	CRGB rgb;

	if (root.containsKey("rgb")) {
	    rgb_r = root["rgb"][0];
	    rgb_g = root["rgb"][1];
	    rgb_b = root["rgb"][2];
	    // SET THE led
	    // if led = 255 set all LEDs to rgb value
	    if (led == 255) {
		for (uint16_t i = 0; i < PIXEL_COUNT; i++) {
		    leds[i] = CRGB(rgb_r, rgb_g, rgb_b);
		}
		FastLED.show();
	    } else if (led < PIXEL_COUNT) {
		leds[led] = CRGB(rgb_r, rgb_g, rgb_b);
		FastLED.show();
	    }
	} else if (root.containsKey("hsv")) {
	    int hsv_h = root["hsv"][0];
	    int hsv_s = root["hsv"][1];
	    int hsv_v = root["hsv"][2];
	    // SET THE led
	    // if led = 255 set all LEDs to hsv value
	    if (led == 255) {
		for (uint16_t i = 0; i < PIXEL_COUNT; i++) {
		    leds[i] = CHSV(hsv_h, hsv_s, hsv_v);
		}
		FastLED.show();
	    } else if (led < PIXEL_COUNT) {
		leds[led] = CHSV(hsv_h, hsv_s, hsv_v);
		FastLED.show();
	    }
	}
	// DEBUGGING(message);
	// send reply
	HTTP.send(200, "text/plain", json);
    }
}

void handleNotFound()
{
    Serial.println("handleNotFound...");
    HTTP.send(404, "text/plain", "File not found");

}

/*
 *
 * LED utils
 *
 */


// Fill the dots one after the other with a color
void colorWipe(struct CRGB rgb, uint8_t wait)
{
    for (int i = 0; i < PIXEL_COUNT; i++) {
	leds[i] = rgb;
	FastLED.show();
	delay(wait);
    }
}

void setup()
{
    Serial.begin(115200);
    DEBUGGING();
    // LED setup
    FastLED.addLeds < NEOPIXEL, PIXEL_PIN > (leds, PIXEL_COUNT);
    // setup hostname
    sprintf(hostString, "LHAUMPION_%06X", ESP.getChipId());
    WiFi.hostname(hostString);
    DEBUGGING(hostString);
    // wifi setup
    setup_wifi();
    // Set up mDNS responder:
    // - first argument is the domain name, here the fully-qualified
    //   domain name is "value_of(hostString).local"
    // - second argument is the IP address to advertise
    //   we send our IP address on the WiFi network
    if (!MDNS.begin(hostString)) {
	Serial.println("Error setting up MDNS responder!");
	while (1) {
	    delay(1000);
	}
    }
    Serial.println("mDNS responder started");

    // http setup
    HTTP.on("/", handleRoot);
    HTTP.on("/api/", handleAPI);
    HTTP.onNotFound(handleNotFound);
    HTTP.begin();
    Serial.println("HTTP server started");

    // Add service to MDNS-SD
    MDNS.addService("lhaumpion", "tcp", 80);
    MDNS.addServiceTxt("lhaumpion", "tcp", "LED", "PIXEL_COUNT");
    MDNS.addServiceTxt("lhaumpion", "tcp", "API", API_VERSION);

    // LED start sequence
    for (int i = 0; i < 256; i++) {
	leds[0] = (CHSV(i, 255, 255));
	FastLED.show();
	delay(10);
    }
    colorWipe(CRGB::Red, 50);	// Red
    colorWipe(CRGB::Green, 50);	// Green
    colorWipe(CRGB::Blue, 50);	// Blue
    colorWipe(CRGB::Black, 50);	// Black

}

/*
 * main loop
 *
 */

void loop()
{
    HTTP.handleClient();
    MDNS.update();
}
