import QtQuick 2.3
import QtQuick.Controls 1.2

ApplicationWindow {
    width: 800
    height: 600
    title: qsTr("Test color merge")
    visible: true

    Column {
        anchors.fill: parent
        anchors.margins: 10

        spacing: 10

        ColorSlider {
            id: color1
            width: parent.width
        }
        ColorSlider {
            id: color2
            width: parent.width
        }
        ColorSlider {
            id: color3
            width: parent.width
        }

        Rectangle {
            id: result
            height: 40
            width: parent.width
            property real hue
            color: {
                var hue1 = color1.hue
                var hue2 = color2.hue
                var hue3 = color3.hue

                var vecx = Math.cos(hue1*Math.PI*2)
                         + Math.cos(hue2*Math.PI*2)
                         + Math.cos(hue3*Math.PI*2)
                var vecy = Math.sin(hue1*Math.PI*2)
                         + Math.sin(hue2*Math.PI*2)
                         + Math.sin(hue3*Math.PI*2)

                var local_hue = (Math.atan2(vecy, vecx) / 2 / Math.PI + 1) % 1
                hue = local_hue
                return Qt.hsla(local_hue, 1, 0.5, 1)
            }
            Text {
                id: resulthue
                anchors.centerIn: parent
                text: (parent.hue * 360).toFixed(0)
            }
        }

        Canvas {
            width: parent.width
            height: 250

            property real hue1: color1.hue
            property real hue2: color2.hue
            property real hue3: color3.hue

            property real mix: result.hue

            onHue1Changed: requestPaint()
            onHue2Changed: requestPaint()
            onHue3Changed: requestPaint()
            onMixChanged: requestPaint()

            onPaint: {
                var ctx = getContext('2d');
                var cx = width/2
                var cy = height/2
                var r = Math.min(cx, cy)

                ctx.reset()

                ctx.save()
                ctx.fillStyle = "#000"
                ctx.arc(cx, cy, r, 0, 360, false)
                ctx.fill()
                ctx.restore()

                ctx.lineWidth = 5
                ctx.lineCap = 'round'
                r *= 0.9

                ctx.save()
                ctx.strokeStyle = Qt.hsla(hue1, 1, 0.5, 1)
                ctx.beginPath()
                ctx.moveTo(cx, cy)
                ctx.lineTo(cx + r * Math.cos(-hue1 * Math.PI * 2), cy + r * Math.sin(-hue1 * Math.PI * 2))
                ctx.stroke()
                ctx.restore()

                ctx.save()
                ctx.strokeStyle = Qt.hsla(hue2, 1, 0.5, 1)
                ctx.beginPath()
                ctx.moveTo(cx, cy)
                ctx.lineTo(cx + r * Math.cos(-hue2 * Math.PI * 2), cy + r * Math.sin(-hue2 * Math.PI * 2))
                ctx.stroke()
                ctx.restore()

                ctx.save()
                ctx.strokeStyle = Qt.hsla(hue3, 1, 0.5, 1)
                ctx.beginPath()
                ctx.moveTo(cx, cy)
                ctx.lineTo(cx + r * Math.cos(-hue3 * Math.PI * 2), cy + r * Math.sin(-hue3 * Math.PI * 2))
                ctx.stroke()
                ctx.restore()

                r *= 0.5

                ctx.save()
                ctx.strokeStyle = Qt.hsla(mix, 1, 0.5, 1)
                ctx.beginPath()
                ctx.moveTo(cx, cy)
                ctx.lineTo(cx + r * Math.cos(-mix * Math.PI * 2), cy + r * Math.sin(-mix * Math.PI * 2))
                ctx.stroke()
                ctx.restore()
            }
        }
    }
}
