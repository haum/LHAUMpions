import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

RowLayout {
    id: component

    property color color: Qt.hsla(colorSlider.value, 1, 0.5, 1)
    property real hue: colorSlider.value

    Slider {
        Layout.fillWidth: true
        id: colorSlider
    }

    Rectangle {
        width: 80
        height: width
        color: component.color

        Text {
            anchors.centerIn: parent
            text: (hue * 360).toFixed(0)
        }
    }
}
