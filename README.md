LHAUMpions les lampes connectées du projet lampes orbitales

Ce projet est composé de 3 parties :

  - les LHAUMpions : lampes connectées en Wi-Fi. Elles acceptent actuellement des méthodes 'post' en HTTP au format JSON
  - le module *Vision* : module utilisant *OpenCV* avec *Python 2* qui permet la détection sur un fond blanc (à confirmer) des ronds de couleur
    - dans le dossier vision, le fichier vision_test-rebrec.py est actuellement paramétré pour se connecté au module *Coordinator*
  - le module *Coordinator* : module utilisant *Python 3* avec *Qt 5* et *treq (Twisted)*.
  Ce module :
    - affiche une représentation graphique des éléments détectés par le module vision;
    - simule des noeuds (LHAUMpions);
    - commande des LHAUMpions en utilisant leur API (HTTP/JSON);
    - (non implémenté) collecte les LHAUMpions connectés sur le réseau local via MDNS

